import { Injectable, Inject } from '@angular/core';
import { CookieService } from 'angular2-cookie/services/cookies.service';
import { Headers, Http } from '@angular/http';
import { Credentials } from '../authorization/credentials';

@Injectable()
export class AuthService {

    urlFragment: string = "getsecuretoken";
    token: string = 'token';

    constructor(
        private cookieService: CookieService,
        private http: Http,
        @Inject('baseUrl') private baseUrl: string) { }

    login(credentials: Credentials): Promise<boolean> {
        let body = "username=" + credentials.username + "&password=" + credentials.password + "&grant_type=password";
        let headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');

        return this.http.post(this.baseUrl + this.urlFragment, body, { headers: headers })
            .toPromise()
            .then(r => this.handleResponse(r.json()))
            .catch(this.handleError)
    }

    isAuthenticated(): boolean {
        return this.cookieService.get(this.token) != null;
    }

    private handleResponse(response: any): boolean {
        this.cookieService.put(this.token, response.access_token);
        return true;
    }

    private handleError(error: any): Promise<any> {
        console.log(error);
        this.cookieService.remove(this.token);
        return Promise.reject(error.message || error);
    }
}