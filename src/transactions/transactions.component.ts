import { Component, Input, OnInit } from '@angular/core';
import { TransactionHeader } from '../transactions/transaction-header'
import { TransactionService } from '../transactions/transactions.service'
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
    selector: 'transactions',
    templateUrl: 'src/transactions/transactions.component.html'
})
export class TransactionsComponent implements OnInit {
    @Input()
    accountId: string;

    transactions: TransactionHeader[]

    constructor(
        private transactionService: TransactionService,
        private router: Router) {

    }

    ngOnInit() {
        this.transactionService
            .getTransactions(this.accountId)
            .then(r => this.transactions = r);
    }

    remove(transaction: TransactionHeader) {
        this.transactionService
            .removeTransaction(this.accountId, transaction.id)
            .then(r => {
                var i = this.transactions.indexOf(transaction);
                this.transactions.splice(i, 1);
            })
    }

    create() {
        this.router.navigate(['accounts', this.accountId, 'transactions']);
    }
}