import { Component, OnInit } from '@angular/core';
import { AuthService } from '../authorization/auth.service';
import { ToolbarComponent } from '../common/toolbar.component';

@Component({
  selector: 'my-app',
  templateUrl: 'src/app/app.component.html'

})
export class AppComponent {
  isAuthenticated: boolean;

  constructor(private authService: AuthService) {
  }

  ngOnInit() {
    this.isAuthenticated = this.authService.isAuthenticated();
  }
}