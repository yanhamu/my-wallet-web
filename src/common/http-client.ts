import { Injectable, Inject } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { CookieService } from 'angular2-cookie/services/cookies.service';

@Injectable()
export class HttpClient {

    token: string = 'token';

    constructor(
        private http: Http,
        private cookieService: CookieService,
        @Inject('baseUrl') private baseUrl: string) { }

    createAuthorizationHeader(): Headers {
        let headers = new Headers();
        let token = this.cookieService.get(this.token);
        headers.append('Authorization', 'bearer ' + token);
        return headers;
    }

    get(url) {
        let headers = this.createAuthorizationHeader();
        return this.http.get(this.baseUrl + url, { headers: headers });
    }

    post(url, data) {
        let headers = this.createAuthorizationHeader();
        return this.http.post(this.baseUrl + url, data, { headers: headers });
    }

    put(url, data) {
        let headers = this.createAuthorizationHeader();
        return this.http.put(this.baseUrl + url, data, { headers: headers });
    }

    delete(url) {
        let headers = this.createAuthorizationHeader();
        return this.http.delete(this.baseUrl + url, { headers: headers });
    }
}