import { Injectable, Inject } from '@angular/core';
import { AccountHeader } from '../accounts/account-header'
import { Account } from '../accounts/account';
import { HttpClient } from '../common/http-client';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class AccountService {

    private accountsUrl = "accounts"

    constructor(private http: HttpClient) { }

    getHeaders(): Promise<AccountHeader[]> {
        return this.http.get(this.accountsUrl)
            .toPromise()
            .then(response => response.json() as AccountHeader[])
            .catch(this.handleError);
    }

    getAccount(accountId: number) {
        return this.http.get(this.accountsUrl + "/" + accountId.toString())
            .toPromise()
            .then(response => response.json() as Account)
            .catch(this.handleError);
    }

    createAccount(account: Account) {
        return this.http
            .post(this.accountsUrl, account)
            .toPromise()
            .then(r => r.json() as Account)
            .catch(this.handleError);
    }

    updateAccount(account: Account) {
        return this.http
            .put(this.accountsUrl, account)
            .toPromise()
            .then(r => r.json() as Account)
            .catch(this.handleError);
    }

    removeAccount(account: Account) {
        return this.http
            .delete(this.accountsUrl + "\\" + account.id)
            .toPromise()
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}