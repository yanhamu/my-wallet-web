export class Category {
    id: string;
    parentId: string;
    name: string;
    color: string;
}