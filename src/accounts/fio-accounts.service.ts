import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '../common/http-client';
import { FioAccount } from '../accounts/fio-account';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class FioAccountService {
    private accountsUrl = "accounts/fio"

    constructor(private http: HttpClient) { }

    createAccount(account: FioAccount) {
        return this.http
            .post(this.accountsUrl, account)
            .toPromise()
            .then(r => r.json() as FioAccount)
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}