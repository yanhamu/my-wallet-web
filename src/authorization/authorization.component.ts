import { Component, OnInit } from '@angular/core';
import { Credentials } from '../authorization/credentials';
import { AuthService } from '../authorization/auth.service';
import { UserService } from '../users/user.service';
import { CookieService } from 'angular2-cookie/services/cookies.service';
import { Router } from '@angular/router';

@Component({
    selector: 'authorization',
    templateUrl: 'src/authorization/authorization.component.html',
    providers: [AuthService, CookieService, UserService]
})
export class AuthorizationComponent implements OnInit {
    logInCredentials: Credentials;
    registrationCredentials: Credentials;

    constructor(
        private authService: AuthService,
        private userService: UserService,
        private router: Router) {
        this.logInCredentials = new Credentials();
        this.registrationCredentials = new Credentials();
    }

    login(): void {
        this.authService
            .login(this.logInCredentials)
            .then(r => {
                if (r)
                    this.router.navigate(['/dashboard']);
            })
    }


    register(): void {
        this.userService.register(this.registrationCredentials);
    }

    ngOnInit() {
        if (this.authService.isAuthenticated())
            this.router.navigate(['/dashboard']);
    }
}