import { Component, OnInit } from '@angular/core';
import { CategoriesService } from '../categories/categories.service';
import { Category } from '../categories/category';

@Component({
    selector: 'categories',
    templateUrl: 'src/categories/categories.component.html',
    providers: [CategoriesService]
})
export class CategoriesComponent implements OnInit {
    categories: Category[];
    treeOptions: any;

    constructor(private categoryService: CategoriesService) {
        this.treeOptions = {
            allowDrag: true,
            allowDrop: (element, to) => {
                if (element != null && element.data.id != to.parent.data.id) {
                    return true;
                }
                return false;
            }
        }
    }

    nodeMoved(eventName) {
        console.log(eventName);
        if (eventName.to.parent.virtual) {
            eventName.node.parentId = null;
        } else {
            eventName.node.parentId = eventName.to.parent.id;
        }
        this.saveCategory(eventName.node);
    }

    ngOnInit() {
        this.categoryService
            .getCategories()
            .then(c => this.categories = c);
    }

    createCategory() {
        let newCategory = new Category();
        newCategory.name = "new category";
        newCategory.color = "";

        this.categoryService
            .createCategory(newCategory)
            .then(c => this.categories.push(c));
    }

    saveCategory(category: Category) {
        this.categoryService
            .updateCategory(category)
    }
}