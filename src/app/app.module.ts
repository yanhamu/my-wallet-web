import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { APP_BASE_HREF } from '@angular/common';
import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { ColorPickerModule } from 'angular2-color-picker';

import { ToolbarComponent } from '../common/toolbar.component';
import { DashboardComponent } from '../dashboard/dashboard.component';
import { TransactionsComponent } from '../transactions/transactions.component';
import { TransactionDetailComponent } from '../transactions/transaction-detail.component';
import { AccountsComponent } from '../accounts/accounts.component';
import { NewAccountComponent } from '../accounts/new-account.component';
import { CustomAccountComponent } from '../accounts/custom-account.component';
import { FioAccountComponent } from '../accounts/fio-account.component';
import { AccountDetailComponent } from '../accounts/account-detail.component';
//import { CategoriesComponent } from '../categories/categories.component';
import { AuthorizationComponent } from '../authorization/authorization.component';

import { AuthService } from '../authorization/auth.service';
import { CookieService } from 'angular2-cookie/services/cookies.service';
//import { TreeModule } from 'angular2-tree-component';

import { HttpClient } from '../common/http-client';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpModule,
    ColorPickerModule,
//    TreeModule
  ],
  declarations: [
    ToolbarComponent,
    AppComponent,
    DashboardComponent,
    TransactionsComponent,
    TransactionDetailComponent,
//    CategoriesComponent,
    AccountsComponent,
    NewAccountComponent,
    CustomAccountComponent,
    FioAccountComponent,
    AccountDetailComponent,
    AuthorizationComponent],
  bootstrap: [AppComponent],
  providers: [
    { provide: APP_BASE_HREF, useValue: '/' },
    CookieService,
    { provide: 'baseUrl', useValue: 'http://localhost/MyWallet.Api/api/' },
    HttpClient,
    AuthService
  ]
})
export class AppModule { }