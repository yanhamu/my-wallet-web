import { Component, OnInit } from '@angular/core';
import { AccountService } from '../accounts/accounts.service';
import { TransactionService } from '../transactions/transactions.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Account } from '../accounts/account';
import { TransactionsComponent } from '../transactions/transactions.component';

import 'rxjs/add/operator/switchMap';

@Component({
    selector: 'accountDetail',
    templateUrl: 'src/accounts/account-detail.component.html',
    providers: [AccountService, TransactionService]
})
export class AccountDetailComponent implements OnInit {
    private account: Account;

    constructor(
        private accountService: AccountService,
        private transactionService: TransactionService,
        private route: ActivatedRoute,
        private router: Router) {
        this.account = new Account();
    }

    ngOnInit() {
        this.route.params.subscribe(params => {
            var accountId = params['accountId'];
            this.account.id = accountId;
            if (!this.isDraft) {
                this.accountService
                    .getAccount(accountId)
                    .then((data) => {
                        this.account = data
                    })
            }
        });
    }

    get isDraft(): boolean {
        return this.account.id == "0";
    }

    save() {
        if (this.isDraft) {
            this.accountService
                .createAccount(this.account)
                .then(a => {
                    this.account = a;
                    this.router.navigate(['/accounts', a.id]);
                });
        } else {
            this.accountService.updateAccount(this.account);
        }
    }

    remove() {
        this.accountService.removeAccount(this.account).then(r => {
            this.router.navigate(['/accounts']);
        });
    }
}