import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DashboardComponent } from '../dashboard/dashboard.component';
import { TransactionDetailComponent } from '../transactions/transaction-detail.component';
import { AccountsComponent } from '../accounts/accounts.component';
import { AccountDetailComponent } from '../accounts/account-detail.component';
import { CategoriesComponent } from '../categories/categories.component';
import { AuthorizationComponent } from '../authorization/authorization.component';
import { NewAccountComponent } from '../accounts/new-account.component';

const appRoutes: Routes = [
    {
        path: 'auth',
        component: AuthorizationComponent
    },
    {
        path: 'dashboard',
        component: DashboardComponent
    },
    {
        path: 'accounts/new-account',
        component: NewAccountComponent
    },
    {
        path: 'accounts/:accountId',
        component: AccountDetailComponent,
    },
    {
        path: 'accounts/:accountId/transactions/:transactionId',
        component: TransactionDetailComponent,
    },
    {
        path: 'accounts/:accountId/transactions',
        component: TransactionDetailComponent,
    },
    {
        path: 'accounts',
        component: AccountsComponent
    },
//    {
//        path: 'categories',
//        component: CategoriesComponent
//    },
    {
        path: '',
        redirectTo: '/auth',
        pathMatch: 'full'
    }
];

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes)
    ],
    exports: [RouterModule],
})
export class AppRoutingModule { }