export class Transaction {
    id: string;
    accountId: string;
    amount: number;

    constructor() {
        this.id = null;
        this.accountId = null;
        this.amount = 0;
    }
}