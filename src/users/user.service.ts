import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { Credentials } from '../authorization/credentials';
import { HttpClient } from '../common/http-client';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class UserService {
    urlFragment: string = "users";

    constructor(private http: HttpClient) { }

    register(credentials: Credentials): void {
        this.http.post("users", credentials)
            .toPromise()
            .then(r => this.handleResponse)
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.log(error);
        return Promise.reject(error.message || error);
    }

    private handleResponse(response: any): void {
        console.log('ok');
    }
}