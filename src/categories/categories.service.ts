import { Injectable } from '@angular/core';
import { Category } from '../categories/category'
import { HttpClient } from '../common/http-client';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class CategoriesService {

    private categoriesUrl = "categories";

    constructor(private http: HttpClient) { }

    getCategories(): Promise<Category[]> {
        return this.http.get(this.categoriesUrl)
            .toPromise()
            .then(res => res.json() as Category[])
            .catch(this.handleError);
    }

    getFlat(): Promise<Category[]> {
        return this.http
            .get(this.categoriesUrl)
            .toPromise()
            .then(res => res.json() as Category[])
            .catch(this.handleError);
    }

    createCategory(category: Category): Promise<Category> {
        return this.http
            .post(this.categoriesUrl, category).toPromise()
            .then(res => res.json() as Category)
            .catch(this.handleError);
    }

    updateCategory(category: Category): Promise<Category> {
        return this.http
            .put(this.categoriesUrl + '\\' + category.id, category)
            .toPromise()
            .then(r => r.json() as Category)
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occured', error);
        return Promise.reject(error.message || error);;
    }
}