import { Component } from '@angular/core';
import { Account } from '../accounts/account';
import { AccountService } from '../accounts/accounts.service';
import { Router } from '@angular/router';

@Component({
    selector: 'custom-account',
    templateUrl: 'src/accounts/custom-account.component.html',
    providers: [AccountService]
})
export class CustomAccountComponent {
    private account: Account;

    constructor(
        private accountService: AccountService,
        private router: Router) {
        this.account = new Account();
    }

    create(account: Account) {
        this.accountService
            .createAccount(account)
            .then(r => this.router.navigate(['accounts']));
    }
}