import { Component, OnInit } from '@angular/core';
import { Transaction } from '../transactions/transaction'
import { TransactionService } from '../transactions/transactions.service'
import { CategoriesService } from '../categories/categories.service'
import { Category } from '../categories/category'
import { Router, ActivatedRoute, Params } from '@angular/router';
import 'rxjs/add/operator/switchMap';

@Component({
    templateUrl: 'src/transactions/transaction-detail.component.html',
    providers: [TransactionService, CategoriesService]
})
export class TransactionDetailComponent implements OnInit {
    private transaction: Transaction;
    private categories: Category[];

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private transactionService: TransactionService,
        private categoryService: CategoriesService) {
        this.transaction = new Transaction();
    }

    ngOnInit() {
        this.route.params.subscribe(params => {
            var transactionId = params['transactionId'];
            var accountId = params['accountId'];

            this.transaction.id = transactionId;
            this.transaction.accountId = accountId;

            this.categoryService
                .getFlat()
                .then(r => {
                    this.categories = r;
                });

            if (!this.isDraft) {
                this.transactionService
                    .getTransaction(transactionId, accountId)
                    .then(r => {
                        this.transaction = r;
                    })
            }
        });
    }

    save() {
        if (this.isDraft) {
            this.transactionService
                .createTransaction(this.transaction)
                .then(r => {
                    this.transaction = r;
                    this.router.navigate(['/accounts', r.accountId, 'transactions', r.id])
                })
        } else {
            this.transactionService
                .updateTransaction(this.transaction)
        }
    }

    get isDraft(): boolean {
        return this.transaction.id == null;
    }
}