import { Component, OnInit } from '@angular/core';
import { AccountService } from '../accounts/accounts.service'
import { AccountHeader } from '../accounts/account-header';
import { Router } from '@angular/router';

@Component({
    selector: 'accounts',
    templateUrl: 'src/accounts/accounts.component.html',
    providers: [AccountService]
})

export class AccountsComponent implements OnInit {
    accounts: AccountHeader[];

    constructor(
        private accountService: AccountService,
        private router: Router) { }

    ngOnInit() {
        this.accountService.getHeaders().then(accounts => this.accounts = accounts);
    }

    createNewAccount(){
        this.router.navigate(['/accounts/new-account']);
    }
}