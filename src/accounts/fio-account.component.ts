import { Component } from '@angular/core';
import { FioAccount } from '../accounts/fio-account';
import { FioAccountService } from '../accounts/fio-accounts.service';
import { Router } from '@angular/router';

@Component({
    selector: 'fio-account',
    templateUrl: 'src/accounts/fio-account.component.html',
    providers: [FioAccountService]
})
export class FioAccountComponent {
    private account: FioAccount;

    constructor(
        private fioAccountService: FioAccountService,
        private router: Router) {
        this.account = new FioAccount();
    }

    create(account: FioAccount) {
        this.fioAccountService
            .createAccount(account)
            .then(r => this.router.navigate(['accounts']));
    }
}