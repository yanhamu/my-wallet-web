import { Injectable } from '@angular/core';
import { TransactionHeader } from '../transactions/transaction-header'
import { Transaction } from '../transactions/transaction'
import { HttpClient } from '../common/http-client';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class TransactionService {

    private transactionsUrl = "accounts/{accountId}/transactions";

    constructor(private http: HttpClient) { }

    getTransactions(accountId: string) {
        return this.http.get(this.getUrl(accountId))
            .toPromise()
            .then(r => r.json() as TransactionHeader[])
            .catch(this.handleError)
    }

    getTransaction(transactionId: string, accountId: string) {
        return this.http.get(this.getUrl2(accountId, transactionId))
            .toPromise()
            .then(r => r.json() as Transaction)
            .catch(this.handleError);
    }

    createTransaction(transaction: Transaction) {
        console.log(transaction.amount);
        return this.http.post(this.getUrl(transaction.accountId), transaction)
            .toPromise()
            .then(r => r.json() as Transaction)
            .catch(this.handleError)
    }

    updateTransaction(transaction: Transaction) {
        return this.http.put(this.getUrl(transaction.accountId), transaction)
            .toPromise()
            .then(r => r.json() as Transaction)
            .catch(this.handleError);
    }

    removeTransaction(accountId: string, transactionId: string) {
        return this.http.delete(this.getUrl2(accountId, transactionId))
            .toPromise()
            .catch(this.handleError);
    }

    private getUrl(accountId: string): string {
        return this.transactionsUrl.replace("{accountId}", accountId.toString())
    }

    private getUrl2(accountId: string, transactionId: string) {
        return this.getUrl(accountId) + "/" + transactionId;
    }
    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}