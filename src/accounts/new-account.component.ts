import { Component, OnInit } from '@angular/core';
import { AccountService } from '../accounts/accounts.service';

@Component({
    selector: 'newAccount',
    templateUrl: 'src/accounts/new-account.component.html',
    providers: [AccountService]
})
export class NewAccountComponent {

    accountType: string;

    constructor() {
        this.accountType = "custom";
    }
}